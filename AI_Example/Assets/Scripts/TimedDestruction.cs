﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedDestruction : MonoBehaviour {

    public float lifeSpan; // how long should this item exist?

	// Use this for initialization
	void Start () {
        Destroy(gameObject, lifeSpan);  // Destroy this GameObject after its lifespan ends
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
