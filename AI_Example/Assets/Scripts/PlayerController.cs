﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float soundLevel;
    public LayerMask myLayers;

    // Use this for initialization
    void Start () {        

	}
	
	// Update is called once per frame
	void Update () {

        // Check W and Up Arrow to move in the positive Y direction
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)){
            transform.position += (transform.up * 2 * Time.deltaTime);
   
        }
		// Check S and Down Arrow to move in the negative Y direction
		else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)){
            transform.position -= (transform.up * 2 * Time.deltaTime);// Move our spaceship in the up direction by 1 unit
        }
        // Check A and Left Arrow to rotate to the left
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)){
            transform.position -= (transform.right * 2 * Time.deltaTime);// Move our spaceship in the up direction by 1 unit
        }
        // Check D and Right Arrow to rotate to the right
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)){
            transform.position += (transform.right * 2 * Time.deltaTime);// Move our spaceship in the up direction by 1 unit
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            fireNoise();
        }
    }
    
    private void fireNoise()
    {
        Collider2D[] nearbyEnemies = Physics2D.OverlapCircleAll(new Vector2(transform.position.x, transform.position.y), soundLevel, myLayers);
        for (int i = 0; i < nearbyEnemies.Length; i++)
        {
            EnemyListener currentEnemy = nearbyEnemies[i].gameObject.GetComponent<EnemyListener>() as EnemyListener;
            if (currentEnemy != null)
                currentEnemy.listen(gameObject.transform);
        }

    }

}
