﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyListener : MonoBehaviour {

    public float hearingRange;
    public GameObject ear;

    private bool soundHeard;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    public void listen(Transform location)
    {
        if (Mathf.Abs(Vector3.Distance(transform.position, location.position)) < hearingRange)
            Instantiate(ear, transform.position - Vector3.forward, Quaternion.identity);
    }
}
