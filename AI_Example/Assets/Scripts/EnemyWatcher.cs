﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWatcher : MonoBehaviour {

    public GameObject exclamationPoint;
    public GameObject questionMark;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 8)
        {
            Vector2 location = new Vector2(transform.position.x, transform.position.y);
            Vector2 playerLocation = new Vector2(collision.gameObject.transform.position.x, collision.gameObject.transform.position.y);
            RaycastHit2D collisionInfo = Physics2D.Raycast(location, playerLocation - location);
            if (collisionInfo.transform != null && collisionInfo.transform.gameObject.layer == 8)
            {
                exclamationPoint.SetActive(true);
                questionMark.SetActive(false);
            }
            else
            {
                exclamationPoint.SetActive(false);
                questionMark.SetActive(true);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == 8)
        {
            exclamationPoint.SetActive(false);
            questionMark.SetActive(false);
        }
    }
}
